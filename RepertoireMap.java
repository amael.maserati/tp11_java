import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

public class RepertoireMap implements Repertoire{
    private Map<String, List<String>> repertoire;
    public RepertoireMap() {
        this.repertoire = new HashMap<>();
    }
    @Override
    public void ajouteContact(String nom, String numero) {
        if (repertoire.containsKey(nom)) {
            repertoire.get(nom).add(numero);
        }
        else {
            List<String> liste = new ArrayList<>();
            liste.add(numero);
            repertoire.put(nom, liste);
        }
    }
    @Override
    public void modifieContact(String nom, String nouveauNom) throws PasDeContactException {
        if (repertoire.containsKey(nom)) {
            List<String> numeros = new ArrayList<>();
            this.repertoire.remove(nom);
            repertoire.put(nouveauNom, numeros);
        }
        else throw new PasDeContactException();
    }
    @Override
    public void supprimeContact(String nom) throws PasDeContactException{
        if (repertoire.containsKey(nom)) {
            this.repertoire.remove(nom);
        }
        else throw new PasDeContactException();
    }
    @Override
    public List<String> getNoms() {
        return new ArrayList<>(this.repertoire.keySet());
    }
    @Override
    public List<String> getNoms(int typeDeTri) {
        List<String> liste = new ArrayList<>(this.repertoire.keySet());
        if (typeDeTri == Repertoire.TRI_PAR_ORDRE_ALPHABETIQUE)  {
            Collections.sort(liste);
        }
        else if (typeDeTri == Repertoire.TRI_PAR_NOMBRE_DE_NUMERO) {
            Collections.sort(liste, new ComparateurNombreNumeros());
        }
        return liste;
    }
    @Override
    public List<String> getNumeros(String nom) {
        throw new UnsupportedOperationException("Unimplemented method 'getNumeros'");
    }
    @Override
    public List<String> rechercheNumero(String numero) throws PasDeContactException {
        throw new UnsupportedOperationException("Unimplemented method 'rechercheNumero'");
    }
    @Override
    public void initRepertoire() {
        throw new UnsupportedOperationException("Unimplemented method 'initRepertoire'");
    }
}