import java.util.Map;
import java.util.List;
import java.util.Comparator;

public class ComparateurNombreNumeros implements Comparator<String> {
    private Map<String, List<String>> repertoire;
    public ComparateurNombreNumeros(Map<String, List<String>> rep) {
        this.repertoire = rep;
    } 
    @Override
    public int compare(String p1, String p2) {
        int n1 = repertoire.get(p2).size();
        int n2 = repertoire.get(p1).size();
        return n2 - n1; 
    }
}
